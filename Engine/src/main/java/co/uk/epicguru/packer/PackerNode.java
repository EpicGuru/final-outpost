package co.uk.epicguru.packer;

public class PackerNode {

	private String name;
	
	public PackerNode(String name){
		this.name = name;
	}
	
	public String toString() { return name; }
}